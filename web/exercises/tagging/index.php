<?php

/*
 * Adjust to use cache tagging and invalidate only the changed pages.
 *
 * curl -sD - varnish.lo/exercises/tagging/?filter=apple
 * curl -sD - varnish.lo/exercises/tagging/?filter=orange
 * curl -X POST --data 'applepie=New description' -sD - varnish.lo/exercises/tagging/update.php
 * curl -sD - varnish.lo/exercises/tagging/?filter=apple
 * # below should be a cache hit, even after updating applepie
 * curl -sD - varnish.lo/exercises/tagging/?filter=orange
 */

require('init.php');

if (isset($_GET['filter'])) {
    foreach ($items as $key => $value) {
        if (false === strpos($key, $_GET['filter'])) {
            unset($items[$key]);
        }
    }
}

header('Cache-Control: public, s-maxage=3600');
// TODO: tag this page with the item keys that where used on it

echo date("Y-m-d H:i:s") . "\n";
foreach ($items as $key => $item) {
    echo "$key: $item\n";
}
