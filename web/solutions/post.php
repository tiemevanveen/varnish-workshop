<?php

// A POST to this page should invalidate it
//
// vendor/bin/phpunit --verbose

require('../../vendor/autoload.php');

use FOS\HttpCache\ProxyClient\Varnish;

header('Cache-Control: s-maxage=300');

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $varnish = new Varnish(['http://127.0.0.1:6181'], 'localhost:6181');

    $varnish->purge(dirname($_SERVER['PHP_SELF']) . '/post.php')->flush();
}

echo date("Y-m-d H:i:s") . "\n";
