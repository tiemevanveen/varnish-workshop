<?php

/*
 * Request this page and see that it is cached for 2 minutes by default:
 *
 * curl -sD - varnish.lo/noinfo.php
 */

echo date("Y-m-d H:i:s") . "\n";
