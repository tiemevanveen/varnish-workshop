<?php

header('Cache-Control: public, s-maxage=3600, max-age=0');
if (isset($_COOKIE[session_name()])) {
    setcookie(session_name(), '', time()-3600, '/');
}
?>
<html>
    <head><title>Cookie demo</title></head>
    <body>
        <h1>Static page</h1>
        <a href="form.php">Go to form</a>
    </body>
</html>
