<?php

namespace Tests;

use FOS\HttpCache\Test\VarnishTestCase;

/**
 * Tests against a live Varnish server. Between each test, the cache is cleared.
 *
 * @link http://foshttpcache.readthedocs.org/en/latest/testing-your-application.html
 */
class CacheTest extends VarnishTestCase
{
    public function testDefaultVarnishBehaviour()
    {
        // First response must be a cache miss.
        $response = $this->getResponse('/noinfo.php');
        $this->assertMiss($response);
        
        // Any subsequent responses must be cache hits.
        $response = $this->getResponse('/noinfo.php');
        $this->assertHit($response);
    }
    
    public function testDefaultVarnishBehaviourWithPurge()
    {
        $this->markTestSkipped('Be sure to include invalidation.vcl');
        
        // Repeat the previous test to cache the response.
        $this->testDefaultVarnishBehaviour();
        
        // Do a PURGE request
        $this->getProxyClient()
            ->purge('/noinfo.php')
            ->flush()
        ;
        
        // And assert the next response is a cache miss.
        $response = $this->getResponse('/noinfo.php');
        $this->assertMiss($response);
    }
    
    public function testPostPurgesData()
    {
        $this->markTestSkipped('Make the test pass by editing web/exercises/post.php');

        $this->assertMiss($this->getResponse('/exercises/post.php'));
        $this->assertHit($this->getResponse('/exercises/post.php'));

        $this->getHttpClient()->post('/exercises/post.php')->send();

        $this->assertMiss($this->getResponse('/exercises/post.php'));
    }
}
