C{    
    #include <stdlib.h>
}C

sub vcl_backend_response {
    if (beresp.http.X-Reverse-Proxy-TTL) {
        C{
            char *ttl;
            const struct gethdr_s hdr = { HDR_BERESP, "\024X-Reverse-Proxy-TTL:" };
            ttl = VRT_GetHdr(ctx, &hdr);
            VRT_l_beresp_ttl(ctx, atoi(ttl));
        }C
        unset beresp.http.X-Reverse-Proxy-TTL;
    }
}
